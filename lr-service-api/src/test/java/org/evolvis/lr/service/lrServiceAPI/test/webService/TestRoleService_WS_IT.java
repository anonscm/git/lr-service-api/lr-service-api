/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.webService;


import junit.framework.Assert;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.Calendar;


public class TestRoleService_WS_IT extends Base {
    private RoleService roleService;
    private Role addMe;
    private Role added;
    private static final String endPointAdress =
            tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.protocol")
                    + "://"
                    + tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.lrServiceAPIHost")
                    + "/lr-service-api-ear-lr-service-api/RoleService";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        RoleServiceService roleServiceService = new RoleServiceService();
        roleService = roleServiceService.getRoleServicePort();
        BindingProvider bp = (BindingProvider) roleService;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointAdress);
        addMe = new Role();
        addMe.setName("TestRole");
        addMe.setDescription("Added via lr-service-api during test");
    }

    @Test
    public void testGetKnownTypes() {
        Assert.assertTrue(roleService.getKnownTypes().getItem().size() > 0);
    }

    @Test
    public void testGetRoleByName() throws Exception {
        Role user = roleService.getRoleByName("User", configuration);
        Assert.assertEquals(roleService.getKnownTypes().getItem().get(0), user.getType());
    }

    @Test
    public void testAddRole() throws Exception {
        addMe.setType(roleService.getKnownTypes().getItem().get(0));
        added = roleService.addRole(addMe, configuration);
        Assert.assertEquals(addMe.getName(), added.getName());
        Assert.assertEquals(addMe.getDescription(), added.getDescription());
    }

    @Test(expected = SOAPFaultException.class)
    public void testAddRoleDuplicate() throws Exception {
        addMe.setType(roleService.getKnownTypes().getItem().get(0));
        added = roleService.addRole(addMe, configuration);
        roleService.addRole(addMe, configuration);
    }

    @After
    public void cleanUp() throws Exception {
        if (added != null) {
            roleService.removeRole(added.getRoleId(), configuration);
        }
    }
}
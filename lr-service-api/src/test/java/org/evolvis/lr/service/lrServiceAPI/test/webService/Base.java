/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */
package org.evolvis.lr.service.lrServiceAPI.test.webService;

import org.evolvis.config.PropertyConfig;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.Configuration;
import org.junit.Before;

public class Base {
    Configuration configuration;
    static final org.evolvis.config.Configuration tarentConf = PropertyConfig.getInstance();

    @Before
    public void setUp() throws Exception {
        configuration = new Configuration();
        configuration.setLrServicePath(tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.lrServicePath"));
        configuration.setUserID(tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.userID"));
        configuration.setPassword(tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.password"));
    }
}

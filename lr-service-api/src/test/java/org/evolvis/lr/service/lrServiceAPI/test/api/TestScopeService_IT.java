/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.api;

import org.evolvis.lr.service.lrServiceAPI.Scope;
import org.evolvis.lr.service.lrServiceAPI.ScopeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestScopeService_IT extends Base {
    private ScopeService scopeService;
    private final static String className =
            tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.scopeService.scope.className");
    private final static long classPK =
            tarentConf.getAsLong("org.evolvis.lr.service.lrServiceApi.test.scopeService.scope.classPK");

    @Before
    public void setUp() throws Exception {
        super.setUp();
        scopeService = new ScopeService();
    }

    @Test
    public void testGetScopes() throws Exception {
        Scope scope = scopeService.getScope(className, classPK, configuration);
        Assert.assertNotNull(scope.getClassPK());
        System.out.println("found scope with classname: " + scope.getClassName() + " and classpk: "
                + Long.toString(scope.getClassPK()));
    }
}

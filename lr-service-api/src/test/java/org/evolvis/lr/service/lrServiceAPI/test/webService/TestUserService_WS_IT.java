/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.webService;

import javax.xml.ws.BindingProvider;

import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.User;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.Configuration;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.UserService;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.UserServiceService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TestUserService_WS_IT extends Base {
    private UserService userService;

    private static final String endPointAdress =
            tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.protocol")
                    + "://"
                    + tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.lrServiceAPIHost")
                    + "/lr-service-api-ear-lr-service-api/UserService";
    private long strippedUserID;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        strippedUserID = Long.parseLong(configuration.getUserID().substring(0, configuration.getUserID().indexOf("-")));

        UserServiceService userServiceService = new UserServiceService();
        userService = userServiceService.getUserServicePort();
        BindingProvider bp = (BindingProvider) userService;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointAdress);

    }

    @Test
    public void testGetUserById() throws Exception {
        User user = userService.getUserById(strippedUserID, configuration);
        Assert.assertEquals(strippedUserID, user.getUserId());
    }

    @Test
    public void testGetUserFromRequest() throws Exception {
        User user = userService.getUserFromRequest(configuration);
        Assert.assertEquals(strippedUserID, user.getUserId());
    }
}
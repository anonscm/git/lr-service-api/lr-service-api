/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.api;

import junit.framework.Assert;

import org.evolvis.lr.service.lrServiceAPI.LRServiceApiException;
import org.evolvis.lr.service.lrServiceAPI.Role;
import org.evolvis.lr.service.lrServiceAPI.RoleService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestRoleService_IT extends Base {

    private RoleService roleService;
    private Role addMe;
    private Role added;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        roleService = new RoleService();
        //intialize object for test
        addMe = new Role();
        addMe.setName("TestRole");
        addMe.setDescription("Added via lr-service-api during test");
    }

    @Test
    public void testGetKnownTypes() {
        Assert.assertTrue(roleService.getKnownTypes().length > 0);
    }

    @Test
    public void testGetRoleByName() throws Exception {
        Role user = roleService.getRoleByName("User", configuration);
        Assert.assertEquals(roleService.getKnownTypes()[0], user.getType());
    }

    @Test
    public void testAddRole() throws Exception {
        addMe.setType(roleService.getKnownTypes()[0]);
        added = roleService.addRole(addMe, configuration);
        Assert.assertEquals(addMe.getName(), added.getName());
        Assert.assertEquals(addMe.getDescription(), added.getDescription());
    }

    @Test(expected = LRServiceApiException.class)
    public void testAddRoleDuplicate() throws Exception {
        addMe.setType(roleService.getKnownTypes()[0]);
        added = roleService.addRole(addMe, configuration);
        roleService.addRole(addMe, configuration);
    }

    @After
    public void cleanUp() throws LRServiceApiException {
        if (added != null) {
            roleService.removeRole(added.getRoleId(), configuration);
        }
    }

}

/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.webService;


import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.ws.BindingProvider;
import java.util.Calendar;


public class TestGroupService_WS_IT extends Base {
    private GroupService groupService;
    private Group addMe;
    private Group added;
    private static final String endPointAdress =
            tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.protocol")
                    + "://"
                    + tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.lrServiceAPIHost")
                    + "/lr-service-api-ear-lr-service-api/GroupService";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        GroupServiceService groupServiceService = new GroupServiceService();
        groupService = groupServiceService.getGroupServicePort();
        BindingProvider bp = (BindingProvider) groupService;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointAdress);
        addMe = new Group();
    }

    @Test
    public void testAddGroup() throws Exception {
        String timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());
        addMe.setName("TestGroup_" + timeNow);
        addMe.setDescription("TestGroup_" + timeNow);
        added = groupService.addGroup(addMe.getName(), addMe.getDescription(), configuration);

        junit.framework.Assert.assertEquals(addMe.getName(), added.getName());
        junit.framework.Assert.assertEquals(addMe.getDescription(), added.getDescription());
    }

    @Test
    public void testGetGroupById() throws Exception {
        String timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());
        addMe.setName("TestGroup_" + timeNow);
        addMe.setDescription("TestGroup_" + timeNow);
        addMe = groupService.addGroup(addMe.getName(), addMe.getDescription(), configuration);

        added = groupService.getGroupById(addMe.getGroupId(), configuration);

        junit.framework.Assert.assertEquals(addMe.getName(), added.getName());
        junit.framework.Assert.assertEquals(addMe.getDescription(), added.getDescription());
    }
}
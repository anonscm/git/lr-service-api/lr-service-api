/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.api;

import org.evolvis.lr.service.lrServiceAPI.*;
import org.evolvis.lr.service.lrServiceAPI.AnnouncementService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class TestAnnouncementService_IT extends Base {
    private AnnouncementService announcementsService;
    private Announcement postMe;
    private Announcement posted;
    private GregorianCalendar displayCal;
    private XMLGregorianCalendar today;
    private static final String groupName =
            tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.announcementsService.scope.groupName");

    @Before
    public void setUp() throws Exception {
        super.setUp();
        announcementsService = new AnnouncementService();
        String announcementType = announcementsService.getKnownAnnouncementTypes()[0];
        //intialize announcement object for test
        postMe = new Announcement();
        postMe.setContent("my ws test");
        postMe.setType(tarentConf.get(announcementType));
        displayCal = new GregorianCalendar();
        displayCal.set(Calendar.MINUTE, 0);
        today = DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar());
    }

    @Test
    public void testAddAnnouncement() throws Exception {
        //title
        postMe.setTitle("testAddAnnouncement()");
        postMe.setDisplayDate(displayCal.getTime());
        //post
        posted = announcementsService.addAnnouncementForGroup(postMe, groupName, configuration);

        //check
        Assert.assertEquals(posted.getTitle(), postMe.getTitle());
        Assert.assertEquals(posted.getContent(), postMe.getContent());
        Assert.assertNotNull(posted.getAnnouncementID());
    }

    @Test
    public void testAddAnnouncementWithoutDisplayDate() throws Exception {
        //title
        postMe.setTitle("testAddAnnouncementWithoutDisplayDate()");

        //post
        posted = announcementsService.addAnnouncementForGroup(postMe, groupName, configuration);

        //check
        Calendar postedDisplayDate = Calendar.getInstance();
        postedDisplayDate.setTime(posted.getDisplayDate());
        int postedYear = postedDisplayDate.get(Calendar.YEAR);
        int postedMonth = postedDisplayDate.get(Calendar.MONTH) + 1;
        int postedDay = postedDisplayDate.get(Calendar.DAY_OF_MONTH);

        Assert.assertEquals(posted.getTitle(), postMe.getTitle());
        Assert.assertEquals(posted.getContent(), postMe.getContent());
        Assert.assertEquals(today.getYear(), postedYear);
        Assert.assertEquals(today.getMonth(), postedMonth);
        Assert.assertEquals(today.getDay(), postedDay);
        Assert.assertNotNull(posted.getAnnouncementID());
    }

    @Test
    public void testAddAnnouncementYesterday() throws Exception {

        //calendar
        displayCal.add(GregorianCalendar.DATE, -1);
        postMe.setDisplayDate(displayCal.getTime());

        //title
        postMe.setTitle("testAddAnnouncementYesterday()");

        //post
        posted = announcementsService.addAnnouncementForGroup(postMe, groupName, configuration);

        //check if the date was set to today instead of yesterday
        Calendar postedDisplayDate = Calendar.getInstance();
        postedDisplayDate.setTime(posted.getDisplayDate());
        int day = postedDisplayDate.get(Calendar.DAY_OF_MONTH);

        Assert.assertEquals(today.getDay(), day);
    }

    @Test
    public void testAddAnnouncementTomorrowAt6() throws Exception {

        //calendar
        displayCal.add(GregorianCalendar.DATE, 1);
        displayCal.set(Calendar.HOUR_OF_DAY, 6);
        postMe.setDisplayDate(displayCal.getTime());

        //title
        postMe.setTitle("testAddAnnouncementTomorrowAt6()");

        //post
        posted = announcementsService.addAnnouncementForGroup(postMe, groupName, configuration);

        //check
        Calendar postedDisplayDate = Calendar.getInstance();
        postedDisplayDate.setTime(posted.getDisplayDate());
        int postedHour = postedDisplayDate.get(Calendar.HOUR_OF_DAY);
        int postedDay = postedDisplayDate.get(Calendar.DAY_OF_MONTH);

        Calendar postMeDate = Calendar.getInstance();
        postMeDate.setTime(postMe.getDisplayDate());
        int postMeDay = postMeDate.get(Calendar.DAY_OF_MONTH);

        Assert.assertEquals(6, postedHour + timeZoneCompensationMap(configuration).get("hourDifference"));
        Assert.assertEquals(postedDay, postMeDay + timeZoneCompensationMap(configuration).get("dayDifference"));
    }

    @After
    public void deleteAnnouncement() throws Exception {
        // delete the announcement
        announcementsService.removeAnnouncement(posted.getAnnouncementID(), configuration);
    }

    /**
     * Provides a Map with the offset of days(under key "dayDifference") and hours 
     * (under key "hourDifference") between server and local.<br/>
     * The difference is calculated like so:<br/>
     * hourDifference = RemoteTimeZone.HOUR_OF_DAY - LocalTimeZone.HOUR_OF_DAY);
     * 
     * @param configuration
     * @return
     * @throws Exception
     */
    private Map<String, Integer> timeZoneCompensationMap(Configuration configuration) throws Exception {

        HashMap<String, Integer> returnHash = new HashMap<String, Integer>();
        
        UserService uService = new UserService();
        CompanyService cService = new CompanyService();
        
        Company tempCompany = cService.getCompanyByCompanyID(uService.getUserById(configuration.getStrippedUserID(), 
        														configuration).getCompanyId(),configuration); 

        Calendar CompanyTZ = new GregorianCalendar(TimeZone.getTimeZone(tempCompany.getTimeZoneId()));
        Calendar LocalTZ = new GregorianCalendar(Calendar.getInstance().getTimeZone());

        int hourDifference = CompanyTZ.get(Calendar.HOUR_OF_DAY) - LocalTZ.get(Calendar.HOUR_OF_DAY);
        int dayDifference = CompanyTZ.get(Calendar.DAY_OF_MONTH) - LocalTZ.get(Calendar.DAY_OF_MONTH);

        returnHash.put("hourDifference", hourDifference);
        returnHash.put("dayDifference", dayDifference);


        return returnHash;


    }
}

/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.webService;


import org.evolvis.config.PropertyConfig;
import org.evolvis.lr.service.lrServiceAPI.ScopeService;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.Announcement;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.AnnouncementService;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.AnnouncementServiceService;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.Configuration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.ws.BindingProvider;


public class TestAnnouncementsService_WS_IT extends Base {
    private AnnouncementService announcementsService;
    private Announcement postMe;
    private Announcement posted;
    private static final String endPointAdress =
            tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.protocol")
                    + "://"
                    + tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.lrServiceAPIHost")
                    + "/lr-service-api-ear-lr-service-api/AnnouncementService";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        AnnouncementServiceService announcementsServiceService = new AnnouncementServiceService();
        announcementsService = announcementsServiceService.getAnnouncementServicePort();
        BindingProvider bp = (BindingProvider) announcementsService;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointAdress);
        postMe = new Announcement();
        postMe.setContent("my ws test");
    }

    @Test
    public void testGetKnownTypes() throws Exception {
        Assert.assertNotNull(announcementsService.getKnownAnnouncementTypes().getItem().get(0));
    }

    @Test
    public void testAddAnnouncement() throws Exception {
        postMe.setType(announcementsService.getKnownAnnouncementTypes().getItem().get(0));

        //title
        postMe.setTitle("testAddAnnouncementViaWS()");

        //post
        posted = announcementsService.addAnnouncementForGroup(postMe,
                tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.announcementsService.scope.groupName"),
                configuration);

        //check
        Assert.assertEquals(posted.getTitle(), postMe.getTitle());
        Assert.assertEquals(posted.getContent(), postMe.getContent());
        Assert.assertNotNull(posted.getAnnouncementID());
    }

    @After
    public void cleanUp() throws Exception {
        if (posted != null) {
            announcementsService.removeAnnouncement(posted.getAnnouncementID(), configuration);
        }
    }
}
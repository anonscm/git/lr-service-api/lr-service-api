/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.api;

import java.util.Calendar;

import junit.framework.Assert;

import org.evolvis.lr.service.lrServiceAPI.Group;
import org.evolvis.lr.service.lrServiceAPI.GroupService;
import org.evolvis.lr.service.lrServiceAPI.User;
import org.evolvis.lr.service.lrServiceAPI.UserService;
import org.junit.Before;
import org.junit.Test;

public class TestGroupService_IT extends Base {

    private GroupService groupService;
    private org.evolvis.lr.service.lrServiceAPI.Group addMe;
    private org.evolvis.lr.service.lrServiceAPI.Group added;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        groupService = new GroupService();
        addMe = new Group();
    }

    @Test
    public void testAddGroup() throws Exception {
        String timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());
        addMe.setName("TestGroup_" + timeNow);
        addMe.setDescription("TestGroup_" + timeNow);
        added = groupService.addGroup(addMe.getName(), addMe.getDescription(), configuration);

        Assert.assertEquals(addMe.getName(), added.getName());
        Assert.assertEquals(addMe.getDescription(), added.getDescription());
    }

    @Test
    public void testGetGroupById() throws Exception {
        String timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());
        addMe.setName("TestGroup_" + timeNow);
        addMe.setDescription("TestGroup_" + timeNow);
        addMe = groupService.addGroup(addMe.getName(), addMe.getDescription(), configuration);

        added = groupService.getGroupById(addMe.getGroupId(), configuration);

        Assert.assertEquals(addMe.getName(), added.getName());
        Assert.assertEquals(addMe.getDescription(), added.getDescription());
    }

    @Test
    public void testGetGroup() throws Exception {
        UserService uService = new UserService();
        User user = uService.getUserFromRequest(configuration);
        String timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());
        addMe.setName("TestGroup_" + timeNow);
        addMe.setDescription("TestGroup_" + timeNow);
        addMe = groupService.addGroup(addMe.getName(), addMe.getDescription(), configuration);

        added = groupService.getGroupByName(addMe.getName(), user.getCompanyId(), configuration);

        Assert.assertEquals(addMe.getName(), added.getName());
        Assert.assertEquals(addMe.getDescription(), added.getDescription());
    }

}

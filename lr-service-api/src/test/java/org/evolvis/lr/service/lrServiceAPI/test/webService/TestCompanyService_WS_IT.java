/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.webService;

import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.Company;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.CompanyService;
import org.evolvis.lr.service.lrServiceAPI.test.webService.generatedSources.CompanyServiceService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.ws.BindingProvider;
import java.util.Calendar;


public class TestCompanyService_WS_IT extends Base {
    private CompanyService companyService;
    private final static String webID = "Test";
    private final static String virtualHost = "tarent.de";
    private final static String mx = "intern.tarent.de";
    private static final String endPointAdress =
            tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.protocol")
                    + "://"
                    + tarentConf.get("org.evolvis.lr.service.lrServiceApi.test.ws.lrServiceAPIHost")
                    + "/lr-service-api-ear-lr-service-api/CompanyService";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        CompanyServiceService companyServiceService = new CompanyServiceService();
        companyService = companyServiceService.getCompanyServicePort();
        BindingProvider bp = (BindingProvider) companyService;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointAdress);
    }

    @Test
    public void testCreateCompany() throws Exception {
        String timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());

        String newWebID = timeNow + "_" + webID;
        String newVirtualHost = timeNow + "_" + virtualHost;
        String newMX = timeNow + "_" + mx;
        Company newCompany = companyService.addCompany(newWebID, newVirtualHost, newMX, configuration);

        //check
        Assert.assertEquals(newWebID, newCompany.getWebId());
        Assert.assertEquals(newVirtualHost, newCompany.getVirtualHost());
        Assert.assertEquals(newMX, newCompany.getMx());
    }

    @Test
    public void testUpdateCompany() throws Exception {

        String timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());
        Company newCompany = companyService.addCompany(timeNow + "_" + webID, timeNow + "_" + virtualHost, timeNow + "_" + mx, configuration);
        timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());
        String homeURL = "updated" + timeNow + ".org";
        newCompany.setHomeURL(homeURL);
        newCompany = companyService.updateCompany(newCompany, configuration);

        //check
        Assert.assertEquals(homeURL, newCompany.getHomeURL());
    }

    @Test
    public void testUpdateCompanyTimeZone() throws Exception {
        String timeNow = Long.toString(Calendar.getInstance().getTimeInMillis());
        String newTimeZone = "Australia/Perth";

        Company newCompany = companyService.addCompany(timeNow + "_" + webID, timeNow + "_" + virtualHost, timeNow + "_" + mx, configuration);
        Company updated = companyService.updateTimeZone(newCompany.getWebId(), newTimeZone, configuration);

        //check
        Assert.assertNotNull(updated);
        Assert.assertEquals(newTimeZone, updated.getTimeZoneId());
    }
}
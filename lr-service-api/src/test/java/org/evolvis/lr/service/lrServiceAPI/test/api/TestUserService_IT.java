/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI.test.api;

import org.evolvis.lr.service.lrServiceAPI.User;
import org.evolvis.lr.service.lrServiceAPI.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestUserService_IT extends Base {

    private UserService userService;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        userService = new UserService();
    }

    @Test
    public void testGetUserById() throws Exception {
        User user = userService.getUserById(configuration.getStrippedUserID(), configuration);
        Assert.assertEquals(user.getUserId(), configuration.getStrippedUserID());

    }

    @Test
    public void testGetUserFromRequest() throws Exception {

        User user = userService.getUserFromRequest(configuration);
        Assert.assertEquals(user.getUserId(), configuration.getStrippedUserID());

    }

}

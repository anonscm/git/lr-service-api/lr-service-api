/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

/**
 * Object containing basic configuration parameters
 * for the connection to WS
 */
public class Configuration {
    public static final long LAYOUTID = -1l;
    public static final String ANNOUNCEMENTENTRYTAIL = "Portlet_Announcements_AnnouncementsEntryService";
    public static final String USERSERVICETAIL = "Portal_UserService";
    public static final String GROUPSERVICETAIL = "Portal_GroupService";
    public static final String CLASSNAMESERVICETAIL = "Portal_ClassNameService";
    public static final String ROLESERVICETAIL = "Portal_RoleService";
    public static final String COMPANYSERVICETAIL = "Portal_CompanyService";
    public static final String ADDRESSSERVICETAIL = "Portal_AddressService";
    public static final String EMAILADDRESSSERVICETAIL = "Portal_EmailAddressService";
    public static final String PHONESERVICETAIL = "Portal_PhoneService";
    public static final String WEBSITESERVICETAIL = "Portal_WebsiteService";

    private String serviceHost;
    private String lrServicePath;
    private String userID;
    private String password;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setServiceHost(String serviceHost) {
        this.serviceHost = serviceHost;
    }

    public String getServiceHost() {
        return serviceHost;
    }

    public void setLrServicePath(String lrServicePath) {
        if (!lrServicePath.endsWith("/")) {
            lrServicePath = lrServicePath + "/";
        }
        this.lrServicePath = lrServicePath;
    }

    public String getLrServicePath() {
        return lrServicePath;
    }

    public long getStrippedUserID() {
        return Long.parseLong(userID.substring(0, userID.indexOf("-")));
    }
}


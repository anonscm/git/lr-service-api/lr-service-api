/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.ClassNameSoap;

/**
 * API-Representation of a ClassName-Object derived from lr´s ClassName-Soap-Object
 */
public class ClassName {

    private long classNameId;
    private long primaryKey;
    private String className;

    public ClassName() {
    }

    public ClassName(long classNameId, long primaryKey, java.lang.String className) {
        this.classNameId = classNameId;
        this.primaryKey = primaryKey;
        this.className = className;
    }

    public ClassName(ClassNameSoap classNameSoap) {
        this.classNameId = classNameSoap.getClassNameId();
        this.primaryKey = classNameSoap.getPrimaryKey();
        this.className = classNameSoap.getValue();
    }

    public long getClassNameId() {
        return classNameId;
    }

    public void setClassNameId(long classNameId) {
        this.classNameId = classNameId;
    }

    public long getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(long primaryKey) {
        this.primaryKey = primaryKey;
    }

    public java.lang.String getClassName() {
        return className;
    }

    public void setClassName(java.lang.String className) {
        this.className = className;
    }
}
/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.AnnouncementsEntrySoap;

import java.util.Date;

/**
 * API-Representation of an Announcement derived from lr´s AnnouncementsEntry-Soap-Object
 */
public class Announcement {
    public final static String[] knownTypes = new String[]{"general"};

    private String title;
    private String content;
    private String type;
    private Date displayDate;
    private Long announcementID;

    public Announcement() {
    }

    /**
     * Constructor which copies the relevant Information from an AnnouncementsEntrySoap - Instance
     *
     * @param entry
     */
    public Announcement(AnnouncementsEntrySoap entry) {
        this.title = entry.getTitle();
        this.content = entry.getContent();
        this.type = entry.getType();
        this.displayDate = entry.getDisplayDate().getTime();
        this.announcementID = entry.getEntryId();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setDisplayDate(Date displayDate) {
        this.displayDate = displayDate;
    }

    public Date getDisplayDate() {
        return displayDate;
    }

    public Long getAnnouncementID() {
        return announcementID;
    }

    public void setAnnouncementID(Long announcementID) {
        this.announcementID = announcementID;
    }
}



/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

public class LRServiceApiException extends Exception {

    public LRServiceApiException() {
    }

    private LRServiceApiException(String message) {
        super(message);
    }

    private LRServiceApiException(String message, Throwable t) {
        super(message, t);
    }

    public static LRServiceApiException withMessage(String message) {
        return new LRServiceApiException(message);
    }

    public static LRServiceApiException withException(ServiceException se) {
        String message = "an initialization error occured while trying to connect lr-tunnel-web";
        return new LRServiceApiException(message, se);
    }

    public static LRServiceApiException withException(RemoteException re) {
        String message;
        if (re.getMessage().contains("(401)")) {
            message = "Authorisation failed.";
        } else if (re.getMessage().contains("(404)")) {
            message = "URL not found.";
        } else if (re.getMessage().contains("refused")) {
            message = "Connection Refused - Is the server running?";
        } else {
            message = "An exception with the message: " + re.getMessage() + " occured. This may have happened due to"
                    + " several reasons as eg. the user might not have the necessary privileges or because of data"
                    + " integrity (you did try to add something that already exists). Please check server log"
                    + " for details";
        }
        return new LRServiceApiException(message, re);
    }
}

/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import java.rmi.RemoteException;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.CompanyServiceSoap;
import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.CompanySoap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides methods for remote management of {@link Company} records
 */
@Stateless
@WebService(targetNamespace = "http://org.evolvis.lr.service.lrServiceAPI.CompanyService/1")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class CompanyService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Adds an Company and returns the fully featured Company-Object
     *
     * @param webId
     * @param virtualHost
     * @param mx
     * @param configuration
     * @return
     * @throws LRServiceApiException
     */
    @WebMethod
    public Company addCompany(String webId, String virtualHost, String mx, Configuration configuration) throws LRServiceApiException {

        // avoid duplicate errors
        try {
            if (getCompanyByWebID(webId, configuration) != null) {
                throw LRServiceApiException.withMessage("A company with the webID: " + webId + " already exists.");
            }
        } catch (LRServiceApiException e) {
            // an exception is expected, so ignore it if its the one telling us that the company does not exist
            if (!e.getMessage().contains("No Company exists")) {
                throw e;
            }
        }

        CompanyServiceSoap service = ServiceProvider.getCompanyServiceSoap(configuration);
        CompanySoap companySoap;
        try {
            companySoap = service.addCompany(webId, virtualHost, mx);
        } catch (RemoteException e) {
            throw LRServiceApiException.withException(e);
        }

        return new Company(companySoap, configuration);
    }

    /**
     * Gets a {@link Company} through WS based on its Id
     *
     * @param companyId     The ID  of the desired {@link Company}
     * @param configuration Configuration parameters for the connection
     * @return A new company object given back by WS
     * @throws LRServiceApiException
     */
    @WebMethod
    public Company getCompanyByCompanyID(long companyId, Configuration configuration) throws LRServiceApiException {
        logger.debug("Attempting to getCompany with id " + companyId);
        CompanyServiceSoap companyService = ServiceProvider.getCompanyServiceSoap(configuration);
        try {
            return new Company(companyService.getCompanyById(companyId), configuration);
        } catch (RemoteException re) {
            LRServiceApiException lfre = LRServiceApiException.withException(re);
            logger.debug("Remote Error when getting Company from server: " + lfre.getMessage(), lfre);
            throw lfre;
        }
    }

    /**
     * Gets a company though WS based on its webId
     *
     * @param webId         The Id of the desired {@link Company}
     * @param configuration Configuration parameters for the connection
     * @return The company object with <code>companyId</code>
     * @throws LRServiceApiException
     */
    @WebMethod
    public Company getCompanyByWebID(String webId, Configuration configuration) throws LRServiceApiException {
        CompanyServiceSoap service = ServiceProvider.getCompanyServiceSoap(configuration);
        CompanySoap companySoap;
        try {
            companySoap = service.getCompanyByWebId(webId);
        } catch (RemoteException e) {
            throw LRServiceApiException.withException(e);
        }
        return new Company(companySoap, configuration);
    }

    /**
     * Updates the provided {@link Company} through WS<br>
     * <u><b>Please note:</b></u> the update-call will not work on some liferay-portals (i.e. 5.2.2) without modifications
     * <p/>
     * 1. the given user has to be admin in the given company as liferay doesnt check if the user is Omni-Admin
     * You can fix that by changing updateCompany() in CompanyServiceImpl.java. Simply change
     * if (!roleLocalService.hasUserRole(getUserId(), companyId, RoleConstants.ADMINISTRATOR, true)) {...
     * to something like: if (!isOmniAdmin && !roleLocalService.hasUserRole(getUserId(), ...
     * where  boolean isOmniAdmin =
     * PermissionCheckerFactoryUtil.create(userLocalService.getUser(getUserId()), true).isOmniadmin();
     * <p/>
     * 2. tunnel web does not know and tell about a field "name" for company. this causes an AccountNameException
     * since the field is null. You can fix this in CompanyLocalServiceImpl.java by replacing validate(name) with
     * try {validate(name); } catch (AccountNameException e) {name = company.getWebId();}
     * as liferay does set the name from the webID during internal use, too.
     *
     * @param company       A full-blown company object, with <u>all</u> fields initialised.
     * @param configuration Configuration parameters for the connection.
     * @return The company object with updated properties.
     * @throws LRServiceApiException
     */
    @WebMethod
    public Company updateCompany(Company company, Configuration configuration) throws LRServiceApiException {
        CompanyServiceSoap service = ServiceProvider.getCompanyServiceSoap(configuration);
        CompanySoap companySoap;
        try {
            companySoap = service.updateCompany(company.getCompanyId(), company.getVirtualHost(), company.getMx(),
                    company.getHomeURL(), company.getName(), company.getLegalName(), company.getLegalId(), company.getLegalType(),
                    company.getSicCode(), company.getTickerSymbol(), company.getIndustry(), company.getType(), company.getSize());

        } catch (RemoteException e) {
            throw LRServiceApiException.withException(e);
        }
        return new Company(companySoap, configuration);
    }

    /**
     * Updates the timezone for {@link Company} through WS.<br/>
     * <p/>
     * <u><b>Please note:</b></u> the update-call will not work on some liferay-portals (i.e. 5.2.2) without modifications
     * <p/>
     * 1. the given user has to be admin in the given company as liferay doesnt check if the user is Omni-Admin
     * You can fix that by changing updateCompany(), updateLogo(), updateDisplay(), updateSecurity() and updatePreferences()
     * in CompanyServiceImpl.java. Simply change
     * <code>if (!roleLocalService.hasUserRole(getUserId(), companyId, RoleConstants.ADMINISTRATOR, true)) {...</code>
     * to something like: <code>if (!isOmniAdmin && !roleLocalService.hasUserRole(getUserId(), ...
     * where  boolean isOmniAdmin =
     * PermissionCheckerFactoryUtil.create(userLocalService.getUser(getUserId()), true).isOmniadmin();</code>
     * <p/>
     * 2. tunnel web does not know and tell about a field "name" for company. this causes an AccountNameException
     * since the field is null. You can fix this in CompanyLocalServiceImpl.java by replacing validate(name) with
     * try {validate(name); } catch (AccountNameException e) {name = company.getWebId();}
     * as liferay does set the name from the webID during internal use, too.
     *
     * @param webId         The webId of the {@link Company} to update
     * @param configuration Configuration parameters for the connection.
     * @return Returns true if update is successful
     * @throws LRServiceApiException
     * @throws RemoteException
     */
    @WebMethod
    public Company updateTimeZone(String webId, String timeZoneId, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to updateCompanyTimeZone to '" + timeZoneId + "'");

        Company company = getCompanyByWebID(webId, configuration);
        CompanyServiceSoap companyService = ServiceProvider.getCompanyServiceSoap(configuration);

        try {
            return new Company(companyService.updateCompany(company.getCompanyId(), company.getVirtualHost(), company.getMx(),
                    company.getHomeURL(), company.getName(), company.getLegalName(), company.getLegalId(), company.getLegalType(),
                    company.getSicCode(), company.getTickerSymbol(), company.getIndustry(), company.getType(), company.getSize(),
                    company.getLanguageId(), timeZoneId, company.getAddresses(), company.getEmailAddresses(), company.getPhones(), company.getWebsites(), null), configuration);
        } catch (RemoteException e) {
            throw LRServiceApiException.withException(e);
        }

    }
}

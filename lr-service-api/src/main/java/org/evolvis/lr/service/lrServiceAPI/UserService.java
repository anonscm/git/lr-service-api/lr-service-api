/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import java.rmi.RemoteException;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.UserServiceSoap;
import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.UserSoap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides methods for remote management of <code>User</code> records through WS.
 *
 * @author plafue
 */
@Stateless
@WebService(targetNamespace = "http://org.evolvis.lr.service.lrServiceAPI.UserService/1")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * Attempts to retrieve the user specified in configuration from LR through WS.
     *
     * @param configuration Configuration containing user information
     * @return {@link User}
     * @throws LRServiceApiException
     */
    @WebMethod
    public User getUserFromRequest(Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to getUser");
        return getUserById(configuration.getStrippedUserID(), configuration);
    }

    /**
     * Attempts to retrieve the user with a certain ID through WS.
     *
     * @param userId        Id for the user to retrieve
     * @param configuration Configuration parameters for the connection
     * @return {@link User}
     * @throws LRServiceApiException
     */
    @WebMethod
    public User getUserById(long userId, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to getUser");

        UserSoap user;
        try {
            UserServiceSoap userService = ServiceProvider.getUserServiceSoap(configuration);
            user = userService.getUserById(userId);
        } catch (RemoteException re) {
            LRServiceApiException lfre = LRServiceApiException.withException(re);
            logger.debug("Remote Error when getting User from server: " + lfre.getMessage(), lfre);
            throw lfre;
        }
        return new User(user);
    }

}

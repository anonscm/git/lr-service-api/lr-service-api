/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;


public class Scope {
    private String className;
    private long classNameID;
    private long classPK;

    public Scope() {
    }

    public Scope(ClassName classNameObject, long classPK) {
        this.className = classNameObject.getClassName();
        this.classNameID = classNameObject.getClassNameId();
        this.classPK = classPK;
    }

    public String getClassName() {
        return className;
    }

    public long getClassNameID() {
        return classNameID;
    }

    public long getClassPK() {
        return classPK;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setClassNameID(long classNameID) {
        this.classNameID = classNameID;
    }

    public void setClassPK(long classPK) {
        this.classPK = classPK;
    }
}

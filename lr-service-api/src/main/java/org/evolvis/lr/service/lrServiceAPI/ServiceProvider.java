/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.rpc.ServiceException;

public class ServiceProvider {

    private static final Logger logger = LoggerFactory.getLogger(ServiceProvider.class);

    /**
     * Provides the service needed for group manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static GroupServiceSoap getGroupServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getGroupServiceSoap");
        GroupServiceSoapService service = new GroupServiceSoapServiceLocator();
        ((GroupServiceSoapServiceLocator) service).setPortal_GroupServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.GROUPSERVICETAIL);
        try {
			return service.getPortal_GroupService();
		} catch (ServiceException e) {
            throw LRServiceApiException.withException(e);
		}
    }
    
    /**
     * Provides the service needed for class manipulation through  WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static ClassNameServiceSoap getClassNameServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getClassNameServiceSoap");
        ClassNameServiceSoapService service = new ClassNameServiceSoapServiceLocator();
        ((ClassNameServiceSoapServiceLocator) service).setPortal_ClassNameServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.CLASSNAMESERVICETAIL);
        ClassNameServiceSoap classNameService;
        try {
			classNameService = service.getPortal_ClassNameService();
		} catch (ServiceException e) {
            throw LRServiceApiException.withException(e);
		}
        return classNameService;
    }
    
    /**
     * Provides the service needed for user manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static UserServiceSoap getUserServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getUserServiceSoap");
        UserServiceSoapService uService = new UserServiceSoapServiceLocator();
        ((UserServiceSoapServiceLocator) uService).setPortal_UserServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.USERSERVICETAIL);
        UserServiceSoap userService;
        try {
			userService = uService.getPortal_UserService();
		} catch (ServiceException e) {
            throw LRServiceApiException.withException(e);
		}

        return userService;
    }

    /**
     * Provides the service needed for announcement manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static AnnouncementsEntryServiceSoap getAnnouncementsEntryServiceSoap(Configuration configuration) 
    		throws LRServiceApiException {
    	logger.debug("Attempting to getAnnouncementsEntryServiceSoap");
        AnnouncementsEntryServiceSoapService service = new AnnouncementsEntryServiceSoapServiceLocator();
        ((AnnouncementsEntryServiceSoapServiceLocator) service).setPortlet_Announcements_AnnouncementsEntryServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.ANNOUNCEMENTENTRYTAIL);
        AnnouncementsEntryServiceSoap announcementService;
        try {
            announcementService = service.getPortlet_Announcements_AnnouncementsEntryService();
        } catch (ServiceException se) {
            throw LRServiceApiException.withException(se);
        }
        return announcementService;
    }

    /**
     * Provides the service needed for role manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static RoleServiceSoap getRoleServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getRoleServiceSoap");
        RoleServiceSoapService rService = new RoleServiceSoapServiceLocator();
        ((RoleServiceSoapServiceLocator) rService).setPortal_RoleServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.ROLESERVICETAIL);
        RoleServiceSoap roleService;
        try {
            roleService = rService.getPortal_RoleService();
        } catch (ServiceException se) {
            throw LRServiceApiException.withException(se);
        }

        return roleService;
    }
    
    /**
     * Provides the service needed for company manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static CompanyServiceSoap getCompanyServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getCompanyServiceSoap");
        CompanyServiceSoapService rService = new CompanyServiceSoapServiceLocator();
        ((CompanyServiceSoapServiceLocator) rService).setPortal_CompanyServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.COMPANYSERVICETAIL);
        CompanyServiceSoap companyService;
        try {
            companyService = rService.getPortal_CompanyService();
        } catch (ServiceException se) {
            throw LRServiceApiException.withException(se);
        }

        return companyService;
    }
    
    /**
     * Provides the service needed for address manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static AddressServiceSoap getAddressServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getCompanyServiceSoap");
        AddressServiceSoapService rService = new AddressServiceSoapServiceLocator();
        ((AddressServiceSoapServiceLocator) rService).setPortal_AddressServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.ADDRESSSERVICETAIL);
        AddressServiceSoap addressService;
        try {
        	addressService = rService.getPortal_AddressService();
        } catch (ServiceException se) {
            throw LRServiceApiException.withException(se);
        }

        return addressService;
    }
    
    /**
     * Provides the service needed for E-Mail address manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static EmailAddressServiceSoap getEmailAddressServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getCompanyServiceSoap");
        EmailAddressServiceSoapService rService = new EmailAddressServiceSoapServiceLocator();
        ((EmailAddressServiceSoapServiceLocator) rService).setPortal_EmailAddressServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.EMAILADDRESSSERVICETAIL);
        EmailAddressServiceSoap emailAddressService;
        try {
        	emailAddressService = rService.getPortal_EmailAddressService();
        } catch (ServiceException se) {
            throw LRServiceApiException.withException(se);
        }

        return emailAddressService;
    }
    
    /**
     * Provides the service needed for phones manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static PhoneServiceSoap getPhoneServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getCompanyServiceSoap");
        PhoneServiceSoapService rService = new PhoneServiceSoapServiceLocator();
        ((PhoneServiceSoapServiceLocator) rService).setPortal_PhoneServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.PHONESERVICETAIL);
        PhoneServiceSoap phoneService;
        try {
        	phoneService = rService.getPortal_PhoneService();
        } catch (ServiceException se) {
            throw LRServiceApiException.withException(se);
        }

        return phoneService;
    }
    
    /**
     * Provides the service needed for website manipulation through WS
     * @param configuration Configuration parameters for the connection
     * @return
     * @throws ServiceException
     */
    public static WebsiteServiceSoap getWebsiteServiceSoap(Configuration configuration) throws LRServiceApiException {
    	logger.debug("Attempting to getCompanyServiceSoap");
        WebsiteServiceSoapService rService = new WebsiteServiceSoapServiceLocator();
        ((WebsiteServiceSoapServiceLocator) rService).setPortal_WebsiteServiceEndpointAddress(
                "http://" + configuration.getUserID() + ":" + configuration.getPassword()
                        + "@" + configuration.getLrServicePath() + "/" + Configuration.WEBSITESERVICETAIL);
        WebsiteServiceSoap websiteService;
        try {
        	websiteService = rService.getPortal_WebsiteService();
        } catch (ServiceException se) {
            throw LRServiceApiException.withException(se);
        }

        return websiteService;
    }
}

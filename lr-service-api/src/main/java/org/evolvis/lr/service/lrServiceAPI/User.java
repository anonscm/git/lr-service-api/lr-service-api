/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.UserSoap;

/**
 * API-Representation of a User derived from lr´s User-Soap-Object
 */
public class User {

    private boolean active;
    private boolean agreedToTermsOfUse;

    private java.lang.String comments;

    private long companyId;

    private long contactId;

    private java.util.Calendar createDate;

    private boolean defaultUser;

    private java.lang.String emailAddress;

    private int failedLoginAttempts;

    private int graceLoginCount;

    private java.lang.String greeting;

    private java.lang.String languageId;

    private java.util.Calendar lastFailedLoginDate;

    private java.util.Calendar lastLoginDate;

    private java.lang.String lastLoginIP;

    private boolean lockout;

    private java.util.Calendar lockoutDate;

    private java.util.Calendar loginDate;

    private java.lang.String loginIP;

    private java.util.Calendar modifiedDate;

    private java.lang.String openId;

    private java.lang.String password;

    private boolean passwordEncrypted;

    private java.util.Calendar passwordModifiedDate;

    private boolean passwordReset;

    private long portraitId;

    private long primaryKey;

    private java.lang.String reminderQueryAnswer;

    private java.lang.String reminderQueryQuestion;

    private java.lang.String screenName;

    private java.lang.String timeZoneId;

    private long userId;

    private java.lang.String uuid;

    public User() {
    }

    public User(UserSoap userSoap) {
        this.active = userSoap.isActive();
        this.agreedToTermsOfUse = userSoap.isAgreedToTermsOfUse();
        this.comments = userSoap.getComments();
        this.companyId = userSoap.getCompanyId();
        this.contactId = userSoap.getContactId();
        this.createDate = userSoap.getCreateDate();
        this.defaultUser = userSoap.isDefaultUser();
        this.emailAddress = userSoap.getEmailAddress();
        this.failedLoginAttempts = userSoap.getFailedLoginAttempts();
        this.graceLoginCount = userSoap.getGraceLoginCount();
        this.greeting = userSoap.getGreeting();
        this.languageId = userSoap.getLanguageId();
        this.lastFailedLoginDate = userSoap.getLastFailedLoginDate();
        this.lastLoginDate = userSoap.getLastLoginDate();
        this.lastLoginIP = userSoap.getLastLoginIP();
        this.lockout = userSoap.isLockout();
        this.lockoutDate = userSoap.getLockoutDate();
        this.loginDate = userSoap.getLoginDate();
        this.loginIP = userSoap.getLoginIP();
        this.modifiedDate = userSoap.getModifiedDate();
        this.openId = userSoap.getOpenId();
        this.password = userSoap.getPassword();
        this.passwordEncrypted = userSoap.isPasswordEncrypted();
        this.passwordModifiedDate = userSoap.getPasswordModifiedDate();
        this.passwordReset = userSoap.isPasswordReset();
        this.portraitId = userSoap.getPortraitId();
        this.primaryKey = userSoap.getPrimaryKey();
        this.reminderQueryAnswer = userSoap.getReminderQueryAnswer();
        this.reminderQueryQuestion = userSoap.getReminderQueryQuestion();
        this.screenName = userSoap.getScreenName();
        this.timeZoneId = userSoap.getTimeZoneId();
        this.userId = userSoap.getUserId();
        this.uuid = userSoap.getUuid();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAgreedToTermsOfUse() {
        return agreedToTermsOfUse;
    }

    public void setAgreedToTermsOfUse(boolean agreedToTermsOfUse) {
        this.agreedToTermsOfUse = agreedToTermsOfUse;
    }

    public java.lang.String getComments() {
        return comments;
    }

    public void setComments(java.lang.String comments) {
        this.comments = comments;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public java.util.Calendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(java.util.Calendar createDate) {
        this.createDate = createDate;
    }

    public boolean isDefaultUser() {
        return defaultUser;
    }

    public void setDefaultUser(boolean defaultUser) {
        this.defaultUser = defaultUser;
    }

    public java.lang.String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(java.lang.String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getFailedLoginAttempts() {
        return failedLoginAttempts;
    }

    public void setFailedLoginAttempts(int failedLoginAttempts) {
        this.failedLoginAttempts = failedLoginAttempts;
    }

    public int getGraceLoginCount() {
        return graceLoginCount;
    }

    public void setGraceLoginCount(int graceLoginCount) {
        this.graceLoginCount = graceLoginCount;
    }

    public java.lang.String getGreeting() {
        return greeting;
    }

    public void setGreeting(java.lang.String greeting) {
        this.greeting = greeting;
    }

    public java.lang.String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(java.lang.String languageId) {
        this.languageId = languageId;
    }

    public java.util.Calendar getLastFailedLoginDate() {
        return lastFailedLoginDate;
    }

    public void setLastFailedLoginDate(java.util.Calendar lastFailedLoginDate) {
        this.lastFailedLoginDate = lastFailedLoginDate;
    }

    public java.util.Calendar getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(java.util.Calendar lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public java.lang.String getLastLoginIP() {
        return lastLoginIP;
    }

    public void setLastLoginIP(java.lang.String lastLoginIP) {
        this.lastLoginIP = lastLoginIP;
    }

    public boolean isLockout() {
        return lockout;
    }

    public void setLockout(boolean lockout) {
        this.lockout = lockout;
    }

    public java.util.Calendar getLockoutDate() {
        return lockoutDate;
    }

    public void setLockoutDate(java.util.Calendar lockoutDate) {
        this.lockoutDate = lockoutDate;
    }

    public java.util.Calendar getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(java.util.Calendar loginDate) {
        this.loginDate = loginDate;
    }

    public java.lang.String getLoginIP() {
        return loginIP;
    }

    public void setLoginIP(java.lang.String loginIP) {
        this.loginIP = loginIP;
    }

    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public java.lang.String getOpenId() {
        return openId;
    }

    public void setOpenId(java.lang.String openId) {
        this.openId = openId;
    }

    public java.lang.String getPassword() {
        return password;
    }

    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    public boolean isPasswordEncrypted() {
        return passwordEncrypted;
    }

    public void setPasswordEncrypted(boolean passwordEncrypted) {
        this.passwordEncrypted = passwordEncrypted;
    }

    public java.util.Calendar getPasswordModifiedDate() {
        return passwordModifiedDate;
    }

    public void setPasswordModifiedDate(java.util.Calendar passwordModifiedDate) {
        this.passwordModifiedDate = passwordModifiedDate;
    }

    public boolean isPasswordReset() {
        return passwordReset;
    }

    public void setPasswordReset(boolean passwordReset) {
        this.passwordReset = passwordReset;
    }

    public long getPortraitId() {
        return portraitId;
    }

    public void setPortraitId(long portraitId) {
        this.portraitId = portraitId;
    }

    public long getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(long primaryKey) {
        this.primaryKey = primaryKey;
    }

    public java.lang.String getReminderQueryAnswer() {
        return reminderQueryAnswer;
    }

    public void setReminderQueryAnswer(java.lang.String reminderQueryAnswer) {
        this.reminderQueryAnswer = reminderQueryAnswer;
    }

    public java.lang.String getReminderQueryQuestion() {
        return reminderQueryQuestion;
    }

    public void setReminderQueryQuestion(java.lang.String reminderQueryQuestion) {
        this.reminderQueryQuestion = reminderQueryQuestion;
    }

    public java.lang.String getScreenName() {
        return screenName;
    }

    public void setScreenName(java.lang.String screenName) {
        this.screenName = screenName;
    }

    public java.lang.String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(java.lang.String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public java.lang.String getUuid() {
        return uuid;
    }

    public void setUuid(java.lang.String uuid) {
        this.uuid = uuid;
    }
}

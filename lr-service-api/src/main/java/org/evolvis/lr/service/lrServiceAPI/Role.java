/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.RoleSoap;

/**
 * API-Representation of a Role derived from lr´s Role-Soap-Object
 */
public class Role {
    public static enum knownTypes {
        GENERAL,
        COMMUNITY,
        ORGANIZATION
    }

    private String description;
    private String name;

    private long classNameId;
    private long classPK;
    private long companyId;
    private long primaryKey;
    private long roleId;
    private java.lang.String subtype;
    private java.lang.String title;
    private knownTypes type;

    public Role() {
    }

    /**
     * This constructor will only populate the minimum number of fields
     * based on a RoleSoap object.
     *
     * @param roleSoap
     */
    public Role(RoleSoap roleSoap) {
        this.name = roleSoap.getName();
        this.description = roleSoap.getDescription();
        this.type = knownTypes.values()[roleSoap.getType() - 1];
        this.roleId = roleSoap.getRoleId();
    }

    /**
     * This constructor will only populate the minimum number of fields
     *
     * @param name        Name of the role
     * @param description Description for the role
     * @param type        Type of role
     */
    public Role(String name, String description, String type) {
        this.name = name;
        this.description = description;
        this.type = knownTypes.valueOf(type);
    }

    public long getClassNameId() {
        return classNameId;
    }

    public void setClassNameId(long classNameId) {
        this.classNameId = classNameId;
    }

    public long getClassPK() {
        return classPK;
    }

    public void setClassPK(long classPK) {
        this.classPK = classPK;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public long getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(long primaryKey) {
        this.primaryKey = primaryKey;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public java.lang.String getSubtype() {
        return subtype;
    }

    public void setSubtype(java.lang.String subtype) {
        this.subtype = subtype;
    }

    public java.lang.String getTitle() {
        return title;
    }

    public void setTitle(java.lang.String title) {
        this.title = title;
    }

    public String getType() {
        return type.name();
    }

    int getTypeOrdinal() {
        return type.ordinal() + 1;
    }

    public void setType(String type) {
        this.type = knownTypes.valueOf(type);
    }

}

/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import java.rmi.RemoteException;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.ClassNameServiceSoap;
import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.ClassNameSoap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Provides methods for remote management of <code>ClassName</code> records
 */
@Stateless
@WebService(targetNamespace = "http://org.evolvis.lr.service.lrServiceAPI.ClassNameService/1")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class ClassNameService {
    private final Logger logger = LoggerFactory.getLogger(ClassNameService.class);

    /**
     * Gets a {@link ClassName} object through WS based on the {@link ClassName}'s name
     *
     * @param className     The desired {@link ClassName} object
     * @param configuration Configuration parameters for the connection
     * @return {@link ClassName}
     * @throws LRServiceApiException
     */
    public ClassName getClassName(String className, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to getClass with className: '" + className + "'");

        ClassNameSoap classNameSoap;
        try {
            ClassNameServiceSoap service = ServiceProvider.getClassNameServiceSoap(configuration);
            classNameSoap = service.getClassName(className);
        } catch (RemoteException re) {
            LRServiceApiException lfre = LRServiceApiException.withException(re);
            logger.debug("Remote Error when getting Class from server: " + lfre.getMessage(), lfre);
            throw lfre;
        }
        return new ClassName(classNameSoap);
    }
}

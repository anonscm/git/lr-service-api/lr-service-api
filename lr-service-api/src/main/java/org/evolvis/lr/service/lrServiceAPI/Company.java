/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */


package org.evolvis.lr.service.lrServiceAPI;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.*;

import java.rmi.RemoteException;

/**
 * API-Representation of a Company (Instance) derived from lr´s Company-Soap-Object
 */
public class Company {
    private long companyId;
    private java.lang.String virtualHost;
    private java.lang.String webId;
    private long accountId;
    private java.lang.String homeURL;
    private java.lang.String key;
    private long logoId;
    private java.lang.String mx;
    private long primaryKey;
    private java.lang.String name;
    private java.lang.String legalName;
    private java.lang.String legalId;
    private java.lang.String legalType;
    private java.lang.String sicCode;
    private java.lang.String tickerSymbol;
    private java.lang.String industry;
    private java.lang.String type;
    private java.lang.String size;
    private java.lang.String languageId;
    private AddressSoap[] addresses;
    private EmailAddressSoap[] emailAddresses;
    private PhoneSoap[] phones;
    private WebsiteSoap[] websites;
    private java.lang.String timeZoneId;

    public Company() {
    }

    public Company(CompanySoap company, Configuration configuration) throws LRServiceApiException {
        this.companyId = company.getCompanyId();
        this.virtualHost = company.getVirtualHost();
        this.webId = company.getWebId();
        this.accountId = company.getAccountId();
        this.homeURL = company.getHomeURL();
        this.key = company.getKey();
        this.logoId = company.getLogoId();
        this.mx = company.getMx();
        this.primaryKey = company.getPrimaryKey();
        this.name = company.getWebId();
        setCompanyTimeAndLanguage(company.getCompanyId(), company.getMx(), configuration);
        this.setAddresses(getCompanyAddresses(company.getWebId(), company.getMx(), configuration));
        this.setEmailAddresses(getCompanyEmailAddresses(company.getWebId(), company.getMx(), configuration));
        this.setPhones(getCompanyPhones(company.getWebId(), company.getMx(), configuration));
        this.setWebsites(getCompanyWebSites(company.getWebId(), company.getMx(), configuration));
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public java.lang.String getHomeURL() {
        return homeURL;
    }

    public void setHomeURL(java.lang.String homeURL) {
        this.homeURL = homeURL;
    }

    public java.lang.String getKey() {
        return key;
    }

    public void setKey(java.lang.String key) {
        this.key = key;
    }

    public long getLogoId() {
        return logoId;
    }

    public void setLogoId(long logoId) {
        this.logoId = logoId;
    }

    public java.lang.String getMx() {
        return mx;
    }

    public void setMx(java.lang.String mx) {
        this.mx = mx;
    }

    public long getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(long primaryKey) {
        this.primaryKey = primaryKey;
    }

    public java.lang.String getVirtualHost() {
        return virtualHost;
    }

    public void setVirtualHost(java.lang.String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public java.lang.String getWebId() {
        return webId;
    }

    public void setWebId(java.lang.String webId) {
        this.webId = webId;
    }


    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getLegalName() {
        return legalName;
    }

    public void setLegalName(java.lang.String legalName) {
        this.legalName = legalName;
    }

    public java.lang.String getLegalId() {
        return legalId;
    }

    public void setLegalId(java.lang.String legalId) {
        this.legalId = legalId;
    }

    public java.lang.String getLegalType() {
        return legalType;
    }

    public void setLegalType(java.lang.String legalType) {
        this.legalType = legalType;
    }

    public java.lang.String getSicCode() {
        return sicCode;
    }

    public void setSicCode(java.lang.String sicCode) {
        this.sicCode = sicCode;
    }

    public java.lang.String getTickerSymbol() {
        return tickerSymbol;
    }

    public void setTickerSymbol(java.lang.String tickerSymbol) {
        this.tickerSymbol = tickerSymbol;
    }

    public java.lang.String getIndustry() {
        return industry;
    }

    public void setIndustry(java.lang.String industry) {
        this.industry = industry;
    }

    public java.lang.String getType() {
        return type;
    }

    public void setType(java.lang.String type) {
        this.type = type;
    }

    public java.lang.String getSize() {
        return size;
    }

    public void setSize(java.lang.String size) {
        this.size = size;
    }

    public java.lang.String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(java.lang.String languageId) {
        this.languageId = languageId;
    }

    public AddressSoap[] getAddresses() {
        return addresses;
    }

    public void setAddresses(AddressSoap[] addresses) {
        this.addresses = addresses;
    }

    public EmailAddressSoap[] getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(EmailAddressSoap[] emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public PhoneSoap[] getPhones() {
        return phones;
    }

    public void setPhones(PhoneSoap[] phones) {
        this.phones = phones;
    }

    public WebsiteSoap[] getWebsites() {
        return websites;
    }

    public void setWebsites(WebsiteSoap[] websites) {
        this.websites = websites;
    }

    public java.lang.String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(java.lang.String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    private void setCompanyTimeAndLanguage(long companyId, String mx, Configuration configuration) throws LRServiceApiException {

        UserServiceSoap userService = ServiceProvider.getUserServiceSoap(configuration);
        UserSoap tempuser;
        try {
            tempuser = userService.getUserByEmailAddress(companyId, "default@" + mx);
        } catch (RemoteException e1) {
            throw LRServiceApiException.withException(e1);
        }
        this.languageId = tempuser.getLanguageId();
        this.timeZoneId = tempuser.getTimeZoneId();
    }

    private AddressSoap[] getCompanyAddresses(String webId, String mx, Configuration configuration) throws LRServiceApiException {

        AddressServiceSoap addressService = ServiceProvider.getAddressServiceSoap(configuration);
        ScopeService sService = new ScopeService();
        Scope scope = sService.getScope("com.liferay.portal.model.Account", this.companyId, configuration);

        try {
            return addressService.getAddresses(scope.getClassName(), scope.getClassPK());
        } catch (RemoteException e1) {
            throw LRServiceApiException.withException(e1);
        }
    }

    private EmailAddressSoap[] getCompanyEmailAddresses(String webId, String mx, Configuration configuration) throws LRServiceApiException {

        EmailAddressServiceSoap emailAddressService = ServiceProvider.getEmailAddressServiceSoap(configuration);
        ScopeService sService = new ScopeService();
        Scope scope = sService.getScope("com.liferay.portal.model.Account", this.accountId, configuration);
        try {
            return emailAddressService.getEmailAddresses(scope.getClassName(), scope.getClassPK());
        } catch (RemoteException e1) {
            throw LRServiceApiException.withException(e1);
        }
    }

    private PhoneSoap[] getCompanyPhones(String webId, String mx, Configuration configuration) throws LRServiceApiException {

        PhoneServiceSoap phoneService = ServiceProvider.getPhoneServiceSoap(configuration);
        ScopeService sService = new ScopeService();
        Scope scope = sService.getScope("com.liferay.portal.model.Account", this.accountId, configuration);
        try {
            return phoneService.getPhones(scope.getClassName(), scope.getClassPK());
        } catch (RemoteException e1) {
            throw LRServiceApiException.withException(e1);
        }
    }

    private WebsiteSoap[] getCompanyWebSites(String webId, String mx, Configuration configuration) throws LRServiceApiException {

        WebsiteServiceSoap websiteService = ServiceProvider.getWebsiteServiceSoap(configuration);
        ScopeService sService = new ScopeService();
        Scope scope = sService.getScope("com.liferay.portal.model.Account", this.accountId, configuration);

        try {
            return websiteService.getWebsites(scope.getClassName(), scope.getClassPK());
        } catch (RemoteException e1) {
            throw LRServiceApiException.withException(e1);
        }
    }

}

/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import java.rmi.RemoteException;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.GroupServiceSoap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides methods for remote management of <code>Group</code> records
 */
@Stateless
@WebService(targetNamespace = "http://org.evolvis.lr.service.lrServiceAPI.GroupService/1")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class GroupService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Gets a {@link Group} through WS based on the {@link Group}'s name and companyId
     *
     * @param groupName     Name of the desired {@link Group}
     * @param companyId     Id of the {@link Company} this {@link Group} belongs to
     * @param configuration Configuration parameters for the connection
     * @return {@link Group}
     * @throws LRServiceApiException
     */
    @WebMethod
    public Group getGroupByName(String groupName, long companyId, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to getGroup with groupName: '" + groupName + "'");

        try {
            GroupServiceSoap groupService = ServiceProvider.getGroupServiceSoap(configuration);
            return new Group(groupService.getGroup(companyId, groupName));
        } catch (RemoteException re) {
            LRServiceApiException lfre = LRServiceApiException.withException(re);
            logger.error("Remote error when getting Group from server: " + lfre.getMessage(), re);
            throw lfre;
        }
    }

    /**
     * Gets a {@link Group} through WS based on the {@link Group}'s name and companyId
     *
     * @param groupId       Id of the desired {@link Group}
     * @param configuration Configuration parameters for the connection
     * @return {@link Group}
     * @throws LRServiceApiException
     */
    @WebMethod
    public Group getGroupById(long groupId, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to getGroup with groupId: '" + groupId + "'");

        try {
            GroupServiceSoap groupService = ServiceProvider.getGroupServiceSoap(configuration);
            return new Group(groupService.getGroup(groupId));
        } catch (RemoteException re) {
            LRServiceApiException lfre = LRServiceApiException.withException(re);
            logger.error("Remote error when getting Group from server: " + lfre.getMessage(), re);
            throw lfre;
        }
    }

    /**
     * Creates a {@link Group} through WS
     *
     * @param groupName        Desired name for the {@link Group}
     * @param groupDescription Desired description for the {@link Group}
     * @param configuration    Configuration parameters for the connection
     * @return {@link Group}
     * @throws LRServiceApiException
     */
    @WebMethod
    public Group addGroup(String groupName, String groupDescription, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to addGroup with groupName: '" + groupName + "' and groupDescription: '" + groupDescription + "'");

        try {
            GroupServiceSoap groupService = ServiceProvider.getGroupServiceSoap(configuration);
            return new Group(groupService.addGroup(groupName, groupDescription, 1, "/" + groupName, true));
        } catch (RemoteException re) {
            LRServiceApiException lfre = LRServiceApiException.withException(re);
            logger.error("Remote error when creating Group: " + lfre.getMessage(), re);
            throw lfre;
        }
    }

}

/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.GroupSoap;

/**
 * API-Representation of a Group derived from lr´s Group-Soap-Object
 */
public class Group {

    private boolean active;
    private long classNameId;
    private long classPK;
    private long companyId;
    private long creatorUserId;
    private java.lang.String description;
    private java.lang.String friendlyURL;
    private long groupId;
    private long liveGroupId;
    private java.lang.String name;
    private long parentGroupId;
    private long primaryKey;

    public enum groupType {
        DEFAULT,
        COMMUNITY_OPEN,
        COMMUNITY_RESTRICTED,
        COMMUNITY_CLOSED
    }

    private groupType type;

    private java.lang.String typeSettings;

    public Group() {
        this.type = groupType.COMMUNITY_OPEN;
    }

    public Group(GroupSoap groupSoap) {
        active = groupSoap.isActive();
        classNameId = groupSoap.getClassNameId();
        classPK = groupSoap.getClassPK();
        companyId = groupSoap.getCompanyId();
        groupId = groupSoap.getGroupId();
        name = groupSoap.getName();
        description = groupSoap.getDescription();
        parentGroupId = groupSoap.getParentGroupId();
        primaryKey = groupSoap.getPrimaryKey();
        type = groupType.values()[groupSoap.getType()];
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getClassNameId() {
        return classNameId;
    }

    public void setClassNameId(long classNameId) {
        this.classNameId = classNameId;
    }

    public long getClassPK() {
        return classPK;
    }

    public void setClassPK(long classPK) {
        this.classPK = classPK;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getFriendlyURL() {
        return friendlyURL;
    }

    public void setFriendlyURL(java.lang.String friendlyURL) {
        this.friendlyURL = friendlyURL;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getLiveGroupId() {
        return liveGroupId;
    }

    public void setLiveGroupId(long liveGroupId) {
        this.liveGroupId = liveGroupId;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public long getParentGroupId() {
        return parentGroupId;
    }

    public void setParentGroupId(long parentGroupId) {
        this.parentGroupId = parentGroupId;
    }

    public long getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(long primaryKey) {
        this.primaryKey = primaryKey;
    }

    public int getType() {
        return type.ordinal();
    }

    public void setType(int type) {
        this.type = groupType.values()[type];
    }

    public void setType(groupType type) {
        this.type = type;
    }

    public java.lang.String getTypeSettings() {
        return typeSettings;
    }

    public void setTypeSettings(java.lang.String typeSettings) {
        this.typeSettings = typeSettings;
    }
}

/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import java.rmi.RemoteException;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.RoleServiceSoap;
import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.RoleSoap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Provides methods for remote management of <code>Role</code> records through WS.
 */
@Stateless
@WebService(targetNamespace = "http://org.evolvis.lr.service.lrServiceAPI.RoleService/1")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class RoleService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Gets a {@link Role} through WS based on <code>roleID</code>
     *
     * @param roleID        ID of the desired role
     * @param configuration Configuration parameters for the connection
     * @return The desired {@link Role} object given back by WS
     * @throws LRServiceApiException
     */
    @WebMethod
    public Role getRoleByID(long roleID, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to getRole with id " + roleID);

        RoleSoap role;
        try {
            RoleServiceSoap roleService = ServiceProvider.getRoleServiceSoap(configuration);
            role = roleService.getRole(roleID);
        } catch (RemoteException re) {
            LRServiceApiException lfre = LRServiceApiException.withException(re);
            logger.debug("Remote Error when getting Role from server: " + lfre.getMessage(), lfre);
            throw lfre;
        }
        return new Role(role);
    }

    /**
     * Gets a {@link Role} through WS based on <code>roleName</code>
     *
     * @param roleName      Name of the desired {@link Role}
     * @param configuration Configuration parameters for the connection
     * @return The desired {@link Role} object given back by WS
     * @throws LRServiceApiException
     */
    @WebMethod
    public Role getRoleByName(String roleName, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to getRole with id " + roleName);
        UserService uService = new UserService();
        User user = uService.getUserFromRequest(configuration);
        RoleSoap role;
        try {
            RoleServiceSoap roleService = ServiceProvider.getRoleServiceSoap(configuration);
            role = roleService.getRole(user.getCompanyId(), roleName);
        } catch (RemoteException re) {
            LRServiceApiException lfre = LRServiceApiException.withException(re);
            logger.debug("Remote Error when getting Role from server: " + lfre.getMessage(), lfre);
            throw lfre;
        }
        return new Role(role);
    }


    /**
     * Creates a new role through WS
     *
     * @param role          A {@link Role} object containing <u>at least</u> <code>name</code>, <code>description</code>,<code>type</code>
     * @param configuration Configuration parameters for the connection
     * @return The desired {@link Role} object given back by WS
     * @throws LRServiceApiException
     */
    @WebMethod
    public Role addRole(Role role, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to addRole: '" + role + "'");

        try {
            if (getRoleByName(role.getName(), configuration) != null) {
                throw LRServiceApiException.withMessage("A role with the name " + role.getName() + " already exists.");
            }
        } catch (LRServiceApiException e) {
            if (!e.getMessage().contains("No Role exists")) {
                throw e;
            }
        }

        RoleServiceSoap service = ServiceProvider.getRoleServiceSoap(configuration);
        RoleSoap roleSoap;
        try {
            roleSoap = service.addRole(role.getName(), role.getDescription(), role.getTypeOrdinal());
        } catch (RemoteException e) {
            throw LRServiceApiException.withException(e);
        }

        return new Role(roleSoap);
    }

    /**
     * Deletes a {@link Role} through WS based on its id
     *
     * @param roleId        ID of the {@link Role} to delete
     * @param configuration Configuration parameters for the connection
     * @return boolean
     * @throws LRServiceApiException
     */
    @WebMethod
    public boolean removeRole(long roleId, Configuration configuration) throws LRServiceApiException {

        logger.debug("Attempting to removeRole: '" + roleId + "'");
        RoleServiceSoap service = ServiceProvider.getRoleServiceSoap(configuration);
        try {
            service.deleteRole(roleId);
            return true;
        } catch (RemoteException e) {
            throw LRServiceApiException.withException(e);
        }
    }

    /**
     * Returns known available types for a {@link Role}
     *
     * @return String[]
     * @throws LRServiceApiException
     */

    @WebMethod
    public String[] getKnownTypes() {
        Role.knownTypes[] types = Role.knownTypes.values();
        String[] returnMe = new String[types.length];
        for (int i = 0; i < types.length; i++) {
            returnMe[i] = types[i].name();
        }
        return returnMe;
    }
}

/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import org.evolvis.lr.service.lrServiceAPI.lrServiceClasses.*;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.rmi.RemoteException;
import java.util.Calendar;

/**
 * Provides methods for remote management of <code>Announcement</code> records through WS.
 */
@Stateless
@WebService(targetNamespace = "http://org.evolvis.lr.service.lrServiceAPI.AnnouncementService/1")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class AnnouncementService {


    public AnnouncementService() {
    }

    /**
     * Creates an Announcement
     *
     * @param announcement  An <code>Announcement</code> object to create
     * @param scope         {@link Scope}
     * @param configuration Configuration parameters for the connection
     * @return {@link Announcement}
     * @throws LRServiceApiException
     */
    @WebMethod
    public Announcement addAnnouncementForScope(Announcement announcement, Scope scope, Configuration configuration) throws LRServiceApiException {
        //get calendar for just now
        Calendar now = Calendar.getInstance();

        //get newsobjects display date
        Calendar displayCal = Calendar.getInstance();

        //if the newsobject does not have a display date or if the display date is in the past, 
        //set to now to enable notification (LR does not send notification for announcements in the past)
        if(announcement.getDisplayDate()!=null){
    		displayCal.setTime(announcement.getDisplayDate());        	       	
        }else{
        	displayCal = now;
        }        
        if (now.compareTo(displayCal) > 0) displayCal = now;
        

        //expire one month after display
        Calendar expirationCal = Calendar.getInstance();
        expirationCal.add(Calendar.MONTH, 1);


        //service object
        AnnouncementsEntryServiceSoap service = ServiceProvider.getAnnouncementsEntryServiceSoap(configuration);

        //request
        AnnouncementsEntrySoap entry;
        try {
            entry = service.addEntry(Configuration.LAYOUTID,
                    scope.getClassNameID(),
                    scope.getClassPK(),
                    announcement.getTitle(),
                    announcement.getContent(),
                    "", //url parameter not needed
                    announcement.getType(),
                    displayCal.get(Calendar.MONTH),
                    displayCal.get(Calendar.DAY_OF_MONTH),
                    displayCal.get(Calendar.YEAR),
                    // LR TZ handling seems broken, produces 12h offset
                    displayCal.get(Calendar.HOUR_OF_DAY),
                    displayCal.get(Calendar.MINUTE),
                    expirationCal.get(Calendar.MONTH),
                    expirationCal.get(Calendar.DAY_OF_MONTH),
                    expirationCal.get(Calendar.YEAR),
                    // LR TZ handling seems broken, produces 12h offset
                    expirationCal.get(Calendar.HOUR_OF_DAY),
                    expirationCal.get(Calendar.MINUTE),
                    0, false);
        } catch (RemoteException re) {
            throw LRServiceApiException.withException(re);
        }

        return new Announcement(entry);
    }

    /**
     * Creates an Announcement
     *
     * @param announcement  An <code>Announcement</code> object to create
     * @param className
     * @param classPK
     * @param configuration Configuration parameters for the connection
     * @return {@link Announcement}
     * @throws LRServiceApiException
     */
    @WebMethod
    public Announcement addAnnouncement(Announcement announcement, String className, long classPK, Configuration configuration) throws LRServiceApiException {
        ScopeService scopeService = new ScopeService();
        Scope scope = scopeService.getScope(className, classPK, configuration);
        return addAnnouncementForScope(announcement, scope, configuration);
    }

    @WebMethod
    public Announcement addAnnouncementForGroup(Announcement announcement, String groupName, Configuration configuration) throws LRServiceApiException {
        ScopeService scopeService = new ScopeService();
        Scope scope = scopeService.getGroupScope(groupName, configuration);
        return addAnnouncementForScope(announcement, scope, configuration);
    }

    /**
     * Removes an Announcement
     *
     * @param announcementID
     * @param configuration
     * @throws LRServiceApiException
     */
    @WebMethod
    public void removeAnnouncement(Long announcementID, Configuration configuration) throws LRServiceApiException {
        //service object
        AnnouncementsEntryServiceSoap service = ServiceProvider.getAnnouncementsEntryServiceSoap(configuration);
        try {
            service.deleteEntry(announcementID);
        } catch (RemoteException re) {
            throw LRServiceApiException.withException(re);
        }
    }

    /**
     * Returns an array of Strings representing the Announcement-Types known by the API
     *
     * @return
     */
    @WebMethod
    public String[] getKnownAnnouncementTypes() {
        return Announcement.knownTypes;
    }
}

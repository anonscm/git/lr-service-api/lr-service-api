/**
 * * lr-service-api
 * * Abstraction layer over lr's tunnel-web (Axis) interface.
 * * Copyright (c) 2011 tarent solutions GmbH
 * *
 * * This program is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU Affero General Public License, version 3
 * * as published by the Free Software Foundation.
 * *
 * * This program is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU Affero General Public License for more details.
 * *
 * * You should have received a copy of the GNU Affero General Public License
 * * along with this program.  If not, see <http://www.gnu.org/licenses/
 */

package org.evolvis.lr.service.lrServiceAPI;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Provides methods for remote management of Scopes (combination of <code>className</code>
 * and <code>classPK<code>, as seen in LRs tables)
 */
@Stateless
@WebService(targetNamespace = "http://org.evolvis.lr.service.lrServiceAPI.ScopeService/1")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class ScopeService {
    private static final String GROUPCLASSNAME = "com.liferay.portal.model.Group";
    private static final String ACCOUNTCLASSNAME = "com.liferay.portal.model.Account";

    public ScopeService() {
    }

    /**
     * Returns a {@link Scope}-instance<br/>
     * <p/>
     * Examples:<br/>
     * "com.liferay.portal.model.Group", "name of an existing group" returns the scope of that group<br/>
     * "com.liferay.portal.model.Account", "webID of an existing company" returns the scope of that account
     *
     * @param className
     * @param classPK
     * @param configuration
     * @return {@link Scope}
     * @throws LRServiceApiException
     */
    @WebMethod
    public Scope getScope(String className, long classPK, Configuration configuration) throws LRServiceApiException {
        ClassNameService classNameService = new ClassNameService();
        ClassName classNameObject = classNameService.getClassName(className, configuration);
        return new Scope(classNameObject, classPK);
    }

    /**
     * convenience-method which returns the scope of a group
     *
     * @param groupName
     * @param configuration
     * @return
     */
    @WebMethod
    public Scope getGroupScope(String groupName, Configuration configuration) throws LRServiceApiException {
        GroupService groupService = new GroupService();
        UserService uService = new UserService();
        User user = uService.getUserFromRequest(configuration);
        Group group = groupService.getGroupByName(groupName, user.getCompanyId(), configuration);
        return getScope(GROUPCLASSNAME, group.getClassPK(), configuration);
    }

    /**
     * ToDo: I forgot what the account is, maybe rename the method to getAccountScope if that makes more sense
     * convenience-method which returns the scope of a company/account
     *
     * @param companyWebID
     * @param configuration
     * @return
     * @throws LRServiceApiException
     */
    @WebMethod
    public Scope getCompanyScope(String companyWebID, Configuration configuration) throws LRServiceApiException {
        CompanyService companyService = new CompanyService();
        Company company = companyService.getCompanyByWebID(companyWebID, configuration);
        return getScope(ACCOUNTCLASSNAME, company.getAccountId(), configuration);
    }
}
